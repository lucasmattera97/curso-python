import random
##random.randint(n1,n2) recibe dos parametros, da un aleatorio en ese rango
##ranbom.randrange(n) da un nro aleatorio desde 0 hasta el n pero sin incluirlo

dado1 = random.randint(1, 6)
dado2 = random.randrange(6) + 1  #al aleatorio le suma uno
dado3 = random.randint(1,6)

resultado = dado1 + dado2 + dado3

print("tus dados fueron")
print("*", dado1)
print("*", dado2)
print("*", dado3)
print("\npor un total de: ", resultado)

input("\n ENTER para salir..")
#y si quiero hacer que escriba el numero en letras?