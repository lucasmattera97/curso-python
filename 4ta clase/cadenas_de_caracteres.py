"""pedir una cadena de caracteres
acceder de manera random a un nro x de sus elementos"""
import random


texto = input('enter texto: ')
maximo = len(texto)
minimo = -len(texto)

for i in range(10):
	posicion = random.randrange(minimo, maximo)
	print('texto[', posicion, ']', end='\t') #end= es para no hacer el salto de linea al final del print
	print(texto[posicion])

"""
  0  1  2  3  4  5  6
' p  y  t  h  o  n  3 ' 
 -1 -2 -3 -4 -5 -6 -7
 asi son las maneras de cnotar una lista cualquira, en esta caso de strings 
 """
 #un string es inmutable, no se puede modificar el dato sino que se crea un neuvo objeto
 