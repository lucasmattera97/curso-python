## a partir de un texto ingresado por teclado elimine las vocales del mismo y lo muestre en pantalla

texto = input('ingrese texto: ')
VOCALES = 'aeiou' 
## una convencion no sintactca es que las variables que no se deban modificar se ponen todas en mayusculas, 
## entonces esa variable se convierte en constante
return_ = ''

for letra in texto:
	if letra.lower() not in(VOCALES):
		return_ += letra
		print(return_)

print('-', return_)
