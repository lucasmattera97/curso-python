print('contando: ')
for i in range(10):
	print(i)

print('contando: ')
for i in range(4,10):
	print(i)

print('contando: ')
for i in range(0, 50, 10): 
	print(i)
"""#en este caso el primero es por el que empieza  contar, 
	el segundo es hasta el cual cuenta, aunque sin incluirlo
	el tercero son los saltos que da en la cuenta"""

print('contando: ')
for i in range(10, 0, -1): 
	print(i)

