"""
MANEJO DE ERRORES

empieza a jugar lo que se llama programacion defensiva que es adelantarse a los errores que puedan llegar a pasaro bien
programar soluciones para los posibles errores

try-catch
try-ecxept en python

try:
    num = float(input('ingresar u numero'))
    print(num*)
except:
    print('hubo un error')

si no se cumple lo que hay dentro del try salta al except
y de esta manera esta el codigo 100% cubierto de errores

try:
    num = float(input('ingresar u numero'))
    print(num*)
except ValueError:
    print('hubo un error')

esto va a atrapar unicamente un value error, o sea va a ejecutar el except cuando se de un value error en el try
de la otra manera atrapa cualquier error que se de en el try

try:
    x = float(input('ingresar u numero'))
    y = float(input('ingresar u numero'))
    print(y/x)
except ValueError:
    print('hay que ingresar numeros')
except ZeroDivisionError:
    print('no puede ser 0 el segundo dato')

o tambien se puede escribir como
except (ZeroDivisionError, ValueError)
solo que asi no se puede diferenciar especificamente que error es el equ eesta dando

except ValueError as e
me permite darle una alias al error y poder imprimirlo, imprime el mensaje del error Value en este caso

mientras menos cosas haya en el try mejor, tenemos mas capacidad de controlar el error

try:
    bloque
except:
    bloque
else:
    aca va a venir si corrio el bloque de try, si no entro a ningun except

try:
    bloque
except:
    bloque
else:
    bloque
finally:
    algo que va a hacer si o si, pase lo que pase
"""