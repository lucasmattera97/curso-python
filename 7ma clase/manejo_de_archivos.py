"""
LECTURA DE ARCHIVO


funcion open(nombre de archivo, modo en el que lo abro)
el archivo debe estar en la misma carpeta que el archivo python sobre el que estoy trajabando
se tiene que llamar ese archivo con la extension y todo
la r es de read, solo voy a leer el archivo

archivo = open(bla.txt, 'r')
archivo.close()
close es para cerrar el archivo
y no se puede hacer mas nada con este

archivo.read(int) 
va leyendo de manera secuencial la cantidad int de caracteres del archivo
me deja el cursor donde termino de leer 

read()
sin parametro lee todo el archivo
print(archivo.read())

archivo.readLine()
me lee de a una linea

archivo.readlines()
genera una lista de todas las lineas del archivo
lineas = archivo.readlines()
print(lineas)

archivo.seek(int)
te ubica el puntero a una posicion dada por parametro

si el archivo queda abierto queda en memoria y es posible que se dañe
archivo = open('texto plano.txt', 'r')
"""

"""
ESCRITORUA DE ARCHIVO

open('texto plano.txt', 'w')
la w es para write sobre el archivo, si este archivo ya existe va a pisar todas las variables sobre el archivo con lo 
que ponga

si creo un archivo que nuevo ya se crea en blanco


"""

archivo = open('nuevo.txt', 'w')
archivo.write('bla\n')
archivo.write('uno\n')
archivo.write('dos\n')
archivo.close()

l = open('nuevo.txt', 'r')

print(l.read())


