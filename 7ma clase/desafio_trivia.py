# Desafío Trivia
# Juego trivia que lee un archivo de texto plano

import sys


def abrir_archivo(nombre, modo):
    """Abrir el archivo, manejando el potencial IOError.potencial
    Cerrar el programa (con sys.exit()) si falla al abrirlo.

    Retornar el archivo abierto."""
    return open(nombre, modo)

    


def obtener_siguiente_linea(archivo):
    """Retornar la siguiente linea del archivo de trivia."""
    print(archivo.readLine())
    


def obtener_siguiente_bloque(archivo):
    """Retornar el siguiente bloque de datos del archivo de trivia.

    Se debe obtener:
    * La categoría
    * La pregunta
    * Las respuestas (4)
    * La respuesta correcta (SIN SALTO DE LÍNEA)
    * La explicación

    Formato del bloque a leer:
    <Categoría>
    <Pregunta>
    <Respuesta 1>
    <Respuesta 2>
    <Respuesta 3>
    <Respuesta 4>
    <Nro de respuesta correcta>
    <Explicación>

    Se debe retornar todo separado por comas."""

    lines = archivo.readlines()
    ret=[]

    for nro in range(1,10):
        if len(lines)!=0:
           # print(lines.pop(0))
            ret.append(lines.pop(0))

    print (lines)
    return ret
    
    

def mostrar_bienvenida(titulo):
    """Bienvenida al jugador."""
    print(titulo)


a = open("trivia.txt",'r')
""" 
print(a.readlines())
print(a.readlines() )
 """    
obtener_siguiente_bloque(a)
obtener_siguiente_bloque(a)

def main():


    # Abrir archivo
    a = open("trivia.txt",'r')
    
    # Obtener título
    print(a.readline())

    # Mostrar bienvenida
    mostrar_bienvenida('hola')
    
    # Inicializar puntaje en 0
    p = 0

    # Obtener primer bloque
    b = obtener_siguiente_bloque(a)

    # Mientras que la categoria no esté vacía
    while len(b)!=0:
        # Mostrar categoria
        print(b[2])
        # Mostrar pregunta
        print(b[3])
        
        # Mostrar respuestas
        print(b[4])
        print(b[5])
        print(b[6])
        print(b[7])

        # Solicitar respuesta
        r=input('dame tu rta elijiendo la opcion')

        # Chequear respuesta
        # Informar si fue o no correcta. Sumar un punto si lo fue.
        if r==b[8]:
            p+=1
            print('rta correcta')
        else:
            print('rta incorrecta')

        # Mostrar explicación
            print()

        # Mostrar puntaje actual

        # Obtener bloque siguiente

    # Cerrar archivo

    # Mostrar puntaje final
    pass


#print(obtener_siguiente_linea(a))