"""
tupla de palabras LISTO
elegir una palabra aleatoria LISTO
cant max de pifiadas LISTO
tantos guiones como caracteres de la palabra
cant de errores LISTO
lista con las letras que se adivinaron 
las condiciones son dos que se llegue al limite de errores Y se haya adivinado la palabra
"""
import random

print('--el juego del ahorcado--')

ls = ['''

  

     +---+

     |   |

         |

         |

         |

         |

  =========''', '''

 

    +---+

    |   |

    O   |

        |

        |

        |

  =========''', '''

 

    +---+

    |   |

    O   |

    |   |

        |

        |

  =========''', '''

 

    +---+

    |   |

    O   |

   /|   |

        |

        |

  =========''', '''

 

    +---+

    |   |

    O   |

   /|\  |

        |

        |

  =========''', '''

 

    +---+

    |   |

    O   |

   /|\  |

   /    |

        |

  =========''', '''

 

    +---+

   |   |

    O   |

   /|\  |

   / \  |

        |

  =========''']

PALABRAS = ('ventrilocuo', 'idiosincrasia', 'institucionalizacion', 'cronometro')
PALABRA_ELEGIDA = random.choice(PALABRAS)
letra_elegida = str()

print('\n')	
for l in PALABRA_ELEGIDA:
	print('_', end=' ')

palabra_en_formacion = list()
indSt = 0
pEleg = list(PALABRA_ELEGIDA)

for l in PALABRA_ELEGIDA:
		palabra_en_formacion.append('_')

while (''.join(palabra_en_formacion) != PALABRA_ELEGIDA) and (indSt+1 < len(ls)):
	
	print(ls[indSt])
	letra_elegida = str(input('\n\ninstroduce una letra: '))

	if letra_elegida in pEleg:
		for l in pEleg:
			if l == letra_elegida:
				palabra_en_formacion.insert(pEleg.index(l), l)
				v = palabra_en_formacion.pop(pEleg.index(l)+1)
			
				pEleg.insert(pEleg.index(l), '_')
				b = pEleg.pop(pEleg.index(l))
	else:
		indSt += 1
		
	print('palabra en formacion: ',' '.join(palabra_en_formacion))
	print('\npalabra elegida:      ',' '.join(pEleg))
		
if ''.join(palabra_en_formacion) == PALABRA_ELEGIDA:
	print('adivinaste la palabra')
else:
	print(ls[indSt])
	print('te quedaste sin intentos, la palabra era', PALABRA_ELEGIDA)