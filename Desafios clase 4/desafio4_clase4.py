"""
4. Crear un programa donde la computadora seleccione de manera aleatoria una palabra, y el usuario debe adivinar dicha palabra. 
Para esto, el programa indica cuántas letras tiene la palabra.
Luego, el usuario tiene 5 chances para preguntar si una letra se encuentra en la palabra. 
La computadora puede responder sólo "Sí" o "No". Finalmente, el usuario debe adivinar la palabra.
"""