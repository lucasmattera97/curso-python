"""
3. Mejorar el programa de la palabra mezclada para que cada posible palabra tenga una pista asociada.
Esta pista deberá mostrarse sólo ante la solicitud de quien está jugando. 
Agregar además un sistema de puntuación que recompensa a quien juega (y gana) sin solicitar la pista.

- pistas para palabras CHECK
- recibir una solicitud de usuario	CHECK
- peticion de solicitud muestra pista 	CHECK
- sistema de puntuacion
	- una puntuacion n por intentos sin pista
	- una puntuacion x por intentos con pista
	- 
"""

import random

print('te voy a mostrar una palabra con las letras derordenadas, vos tenes que adivinarla')

PISTA_HOLA = str('es un saludo') 
PISTA_SILLA = str('mueble muy comun')
PISTA_PALA = str('instrumento de trabajo en tierra')
PISTA_CAMPERA = str('abrigo comun')
PISTA_CADENA = str('una.. es tan fuerte como su eslabon mas debil')

PALABRAS_PISTAS = (
	('hola', PISTA_HOLA), 
	('silla', PISTA_SILLA), 
	('pala', PISTA_PALA), 
	('campera', PISTA_CAMPERA), 
	('cadena', PISTA_CADENA)
	)

PALABRA_ELEGIDA = random.choice(PALABRAS_PISTAS)
PALABRA_ELEGIDA_LS = list(PALABRA_ELEGIDA[0])
random.shuffle(PALABRA_ELEGIDA_LS)
PALABRA_ELEGIDA_UNORD = ''.join(PALABRA_ELEGIDA_LS)
print(PALABRA_ELEGIDA_UNORD)
palabra_ingresada = input('adivina la palabra desordenada: ')

while palabra_ingresada.lower() != PALABRA_ELEGIDA[0]:
	palabra_ingresada = input('\nno es la palabra, i0ntenta de nuevo: ')

	respuesta = input('queres la pista?, recibiras menos puntuacion: ')
	if respuesta.lower() == 'si':
		print(PALABRA_ELEGIDA[1])
		palabra_ingresada = input('\na ver si con la pista la adivinas, ingresa la palabra: ')

print('\nadivinaste la palabra elegida')
print(input('presiona ENTER para continuar...'))

"""
tupla = ('b','a')
print(tupla[0])
"""