import random

tablero = {
        0:None,
        1:None,
        2:None,
        3:None,
        4:None,
        5:None,
        6:None,
        7:None,
        8:None
    }

O = 'O'
X = 'X'

humano = None
pc = None

combinaciones_ganadoras = {
        1:[0,1,2],
        2:[3,4,5],
        3:[6,7,8],
        4:[0,3,6],
        5:[1,4,7],
        6:[2,5,8],
        7:[0,4,8],
        8:[2,4,6]      
    }


def buscador_de_celdas_con(simbolo = None): # devuelve en forma de lista todas las celdas que se hayan llenado con el simbolo dado
    # por defecto devuelve las celdas vacias
    ret =[]
    for k in tablero.keys():
        if tablero[k] == simbolo:
            ret = ret.append(k)

    return ret

def ingresar_posicion_de(jugador=None, posicion=None):
    tablero[posicion] = jugador

def hay_tablero_con_combinacion_ganadora(): # devuelve true si en el tablero hay una combinacion ganadora

    return hay_combinacion_ganadora_con_simbolo(O) or hay_combinacion_ganadora_con_simbolo(X)

def hay_combinacion_ganadora_con_simbolo(simbolo): # devuelve true la lista de celdas usadas es una combinacion ganadora
    ls_celdas_usadas = buscador_de_celdas_con(simbolo)
    bool = False
    for v in tablero.values():
        bool = bool or con_mismos_elementos( ls_celdas_usadas, v)
    
    return bool

def simbolo_de_combinacion_ganadora(ls_celdas_usadas): # me dice cual es el simbolo que se usa en las celdas de la combinacion ganadora
    ret = None
    if hay_combinacion_ganadora_con_simbolo(buscador_de_celdas_con(O)):
        ret = O
    elif hay_combinacion_ganadora_con_simbolo(buscador_de_celdas_con(X)):
        ret = X
    
    return ret

def con_mismos_elementos(ls1, ls2): # denota true si todos los elementos de la primer lista estan en la segunda
    bool = True
    for e in ls1:
        bool = bool and (e in ls2)

    return bool        

def asignar_simbolos(): # asigna los simbolos a cada jugador aleatoriamente
    ls = [O,X]
    humano = ls.pop(random.randint(0,1))
    pc = ls[0]

def jugada_pc(): # realiza una jugada de la pc
    ls_libres = buscador_de_celdas_con()
    ingresar_posicion_de(jugador=pc, posicion=random.sample(ls_libres,1))






