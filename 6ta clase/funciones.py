def saludar(nombre):
    print('hola', nombre)

#como usar la funcion
#simplementa llamo a la funcion al final del archivo

def multiplicar(nroA, nroB):
     return nroA*nroB

#saludar('pocho')
#multiplicar(1,2)   
#a = int(input('ingresa un numero: '))
#b = int(input('ingresa otro numero: '))
#print('el resultado de tu multiplicadion es: ', multiplicar(a,b))

def obtener_instrucciones():
    return 'para devolver una funcion se usa primero la palabra clave def'

#------------------------------------------------------------------------
def imprimir_instrucciones(instruccion):
    print(instruccion)

#camel case: holaQueTal 
#snake case: hola_que_tal

#-----------------------------------------------------------------------------------
#imprimit 'feliz cumple [nombre] ya tenes x años'
def saludar_por_cumpleanios(nombre, edad):
    print('feliz cumple ', nombre, 'ya tenes ', edad, 'años')
#--------------------------------------------------------------------------
#especificando, en el llamado de la funcion, a que variable se le asigna que parametro podemos jugar con las 
#posiciones, esto se le llama parametro nombrado, no hace falta poner los parametros en orden
"""
funcion(pa1, pa2)

llamado: funcion(pa2= pepe, pa1= larraz)

func(*args, **kwargs)
*args significa que recibe multiples parametros posicionales pero entra
dentro de la categoria de los nombrados, recibe los parametros en forma
de tupla
dentro de la funcion se llama a la tupla sin el *, o sea solo args
args es el nombre del paramettro, puede ser cualquier nombre, el * es el determinante de los 
multiples parametros

**kwargs: keyworded arguments
es un diccionario
de nuevo determina que es un diccionario los **, despues puedo poner cualquier cosa como el nombre
dentro de la funcion kwargs lo tomo como un diccionario
se pueden llamar solos, o sea func(*args) o func(**kwargs)
parametros posicionales van primero, despues los nombrados 
"""
#--------------------------------------
#alcance de funciones

valor = 10

def leer_global():
    print('variable valor en leer global', valor)

def esconder_global():
    valor=-2
    print('variable valor en esconder global: ', valor)

"""
hacer un tateti,
muestra instrucciones del juego
donde lo que sabemos es que esto se tiene que mostrar
un tablero de 9x9
hago un tablero donde cada casilla tiene una numeracion
combiene mostrar ese tablero en pantalla para ubicar al usuario
determinar quien va primero, si uno o la maquina
crear un tablero vacio
hay que mostrar ese tablero vacio
una vez empezado el juego cuando termina?
o alguien gana o se empata
mientras no pasen ninguna de esas dos cosas
si va el humano tengo que pedirle el movimiento y actualizo el tableros actualizado
sino calculamos el movimiento de la maquina e imprimimos el tablero actualizado
y seteamos el turno para la proxima iteracion
se felicita a quien gano o se declara empate

la maquina tiene que tratar de ganar o en el peor de los casos tratar d eno perder

tablero es euna lista que va de cero a ocho

listado de funciones:
mostrar_instrucciones()
solicitar_nro(texto, minimo=0, maximo=9) esto significa que predetermino lo valores de los parametros pero
despues puedo pisarlos llamando al parametro por el nombre
obtener orden
crear tablero
mostrar tablero(tablero)
obtener movimientos posibles (tablero)
obtener gannador (tablero)
obtener movimiento humano(tablero)
obtener movimiento computadora(tablero)
obtener siguiente turno(turno)

"""
