class HumoresDeGremlin():
    
    def __init__(self, humores):
        self._humores = humores  

    def humor_actual(self, hambre, aburrimiento):
        x = hambre + aburrimiento

        for humor in self._humores:
            if humor.condicion(x):
                return humor.descripcion


class Humor():
    
    def __init__(self, descripcion, minimo=0, maximo=1000):
        self.__maximo = maximo 
        self.__minimo = minimo 
        self.__descripcion = str(descripcion).lower()

    def condicion(self, x):
        condicion1, condicion2 = self.__minimo <=  x , x < self.__maximo

        return condicion1 and condicion2

    @property
    def descripcion(self):
        return self.__descripcion
    

class Gremlin:
    def __init__(self, n):
        self.__hambre = 4
        self.__aburrimiento = 4
        self.__nombre = str(n).title()
        self._humores_de_gremlin = HumoresDeGremlin(self.humores_natales)
        self._humor = self._humores_de_gremlin.humor_actual(self.__hambre,self.__aburrimiento)
        
    @property
    def humores_natales(self):
        return [
            Humor('feliz', minimo=0, maximo=5), 
            Humor('ok', minimo=5, maximo=10), 
            Humor('frustrado', minimo=10, maximo=15), 
            Humor('enojado', minimo=15) ]

    @property
    def hambre(self):
        return self.__hambre

    @property
    def aburrimiento(self):
        return self.__aburrimiento

    @property
    def nombre(self):
        return self.__nombre

    @property
    def humor(self):
        return self._humor

    def hablar(self, unidades=2):

        if self.__aburrimiento >= unidades:
            self.__aburrimiento -= unidades
        else:
            self.__aburrimiento = 0

        print(
            'Soy {} y ahora me siento {}'.format(self.nombre, 
                                                self.humor)
        )
        self.actualizar()


    def comer(self, unidades=2):
        if self.__hambre >= unidades:
            self.__hambre -= unidades
        else:
            self.__hambre = 0
            
        print('BUURRRPPPP. Gracias.')
        self.actualizar()

    def jugar(self, unidades=2):
        if self.__aburrimiento >= unidades:
            self.__aburrimiento -= unidades
        else:
            self.__aburrimiento = 0
            
        print('¡Wiiiii!')
        self.actualizar()

    def pasar_tiempo(self):
        self.__hambre += 1 
        self.__aburrimiento += 1

    def actualizar(self):
        self.pasar_tiempo()
        self._humor = self._humores_de_gremlin.humor_actual(self.__hambre,self.__aburrimiento)


class JardinDeGremlins():

        def __init__(self):
                self._name = input('\n¿Cómo querés llamar a tu Gremlin?: ')
                self._gremlin = Gremlin(self._name)
                self._eleccion = self.cartel_de_opciones()

        def jugar_en_el_jardin(self):
                while(self._eleccion != 0 and self._gremlin.humor != 'enojado'):
                        self.eleccion()
                        self.advertencia()
                        self._eleccion = self.cartel_de_opciones()

                if(self._eleccion == 0 ):
                        print('chauchis')
                elif(self._gremlin.humor == 'enojado'):
                        print('\nAAAARRRRGGGHHH!')
                        print('\ntu gremlin se enojo')

        def advertencia(self):
            if(self._gremlin.humor == 'frustrado'):
                print(str('\npeligro de enojo').title())

        def eleccion(self):
                hablar = 1
                comer = 2
                jugar = 3

                if(self._eleccion == hablar):
                        self._gremlin.hablar()
                elif(self._eleccion == comer):
                        self._gremlin.comer()
                elif(self._eleccion == jugar):
                        self._gremlin.jugar()

        def cartel_de_opciones(self):
                print('\n- Jardin de gremlins')
                return int(
                        input(
        """
        0 - Salir
        1 - Escuchar a tu Gremlin
        2 - Alimentar a tu Gremlin
        3 - Jugar con tu Gremlin

        Opcion: """))