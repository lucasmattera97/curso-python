class Gremlin:
    total = 0
  # este atributo lo maneja directamente la clase, o sea que se puede modificar
  # para para todas las instancias, por cada instancia se puede hacer algo aca
  # se puede acceder a este atributo desde la clase o desde cualquiera de sus instancias


  def __init__(self,nombre,humor):
	  print('nacio un neuvo glremlin')
	  self.__nombre = nombre
	  self.__humor = humor
	  self.__class__.total += 1
	  # esto significa que por cada gremlin que cree se va a sumar uno a la variable
	  # con el class decimos que la clase de la instancia en la que estoy parado
	  # es a la que voy a acceder

  """
  VISIBILIDAD
  doble guion bajo = privado
  ej: self.__var

  """

  # estos se llaman decoradores, decorator
  # los metodos de clase reciben por parametro la clase propiamente, no la instancia
  # hay que colocarle el decorator para que sea metodo de clase
  # es util para casos en los que la creacion de un objeto es mas compleja y 
  # requiere de mas pasos
  @classmethod
  def status(cls):
	  print('cant de gremlins', cls.total)

  #@staticmethod
  # para lo que sirve es para aunar funciones en una clase, no reciben ningun parametro
  # el metodo es una funcion que suelta que ni siquiera pertenece a OOP
  # es un metodo que fue agrupado dentro de una clase, no tiene conocimiento ni de la clase
  # ni de los metodos
  # el static tiene poca utilidad
  # sirve, por ejemplo, cuando se tienen muchas funciones sueltas, o sea no se esta programando en OOP
  # y se quiere empezar a pasar el codigo hacia ese paradigma agrupando las funciones por afinidad
  # en clases pero que siguen siendo funciones sueltas

  @property
  def nombre(self):
    return self.__nombre

  @nombre.setter
  def nombre(self, nombre):
    if nombre == "":
      print("no ingresaste nombre")
    else:
      self.__nombre = nombre
      print('nombre modificado')

  """
  la diferencia entre poner e property y no ponerlo es que esta pensado con una estructura de
  cualquier otro lenguaje, no aprovechamos que estamos usando python
  usar los metodos magicos para gtter y setters ayuda tambien cno el sintax sugar

  el getter y setter nos puede servir para determinar un protocolo de modificacion de esos atributos
  si simplemente defolvemos el atributo no tienen mucho sentido pero es una buena practica igual

  """

  def hablar(self):
  	print('hola soy', self.__nombre)
	  print('me siento ', self.__humor)

# los metodos con esta forma son llamados metodos magicos en python
# esto lo que hace es redefinir el comportamiento nativo que tiene de
# mostrar la direccion de memoria
# esto sirve cuando quiero imprimir el objeto, sin hacer esto
# me va a decir, de manera predeterminada, una direccion de memoria
# aca puedo redefinir lo que quiero que muestre cuando imprima un objeto directamente
  def __str__(self):
	  res = 'objeto gremlin\n'
	  res += 'nombre: ' + self.__nombre

	  return res



"""
en general es una buena practica hacer privados los atributos y los metodos pubicos
dejar privados los atributos ayuda al encapsulamiento y evitar que se rompa
tambien se pueden tener metodos privados pero eso va en preferencia
no nos sirve tener muchos metodos privados ya que no se pueden acceder al
momento de hacer unit test
"""

"""
def main():
  g = Gremlin()
  print(g.hablar())

if __name__ == "__main__":
  main()

esto hace que ejecute solo si llamo al main,
sierve si me quiero importar algo y que no ejecute nada, es como una puerta hacia eso
"""
