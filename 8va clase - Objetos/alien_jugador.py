from abc import ABC, abstractmethod
# abstract based class

class Enemigo:
  def morir(self):
    raise NotImplementedError()
    # raise es para lanzar una escepcion

class EnemigoAbs(ABC):
  @abstractmethod
  def morir(self):
    pass
  """
  si heredo de esta clase es necesario que cada clase implemente el mensaje como le sea conveniente
  pero es obligatorio para las clases que hereden de aca
  clases abstractas no se instancian
  puede tener metodos concretos ademas de los abstractos

  blabña pepe
  """

class Alien(EnemigoAbs):
  """
  def morir(self):
    print("d'ouch")

  una clase heredada de otra va a obtener todas las cosas publicas de la superclase
  alien hereda de enemigo

  aca la clase alien no hace nada, simplemente se trae todos los comportamientos de enemigo

  el problema con esto es que no me obliga a redefinir el metodo
  """
  pass

"""
protegido es un solo guion bajo
"""

class Jugador:
  def reventar(self,enemigo):
    print('el jugador revienta un enemigo')
    enemigo.morir()



"""
hay distintas formas de asegurar que el mensaje que le mando al objeto
lo sepa responder, en este caso que si le digo a un enemigo que muera, este pueda morir
una es usar herencia multiple o clases abstractas
las interfaces, como en java, no existen
"""
