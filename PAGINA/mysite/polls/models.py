from django.db import models


# Create your models here.

class DatosPersonales(models.Model):
  nombre = models.CharField(max_length=20)
  apellido = models.CharField(max_length=20)
  descripcion = models.CharField(max_length=100)
  resumen = models.CharField(max_length=200)
  fecha_nacimiento = models.DateField(auto_now=False, auto_now_add=False)

  def __str__(self):
    return self.nombre

class Hobbies(models.Model):
  descripcion = models.CharField(max_length=50)
  practicante = models.ManyToManyField(DatosPersonales, through='HobbiesDe')

  def __str__(self):
    return self.descripcion


class HobbiesDe(models.Model):
  practicante = models.ForeignKey(DatosPersonales, on_delete=models.CASCADE)
  hobbie = models.ForeignKey(Hobbies, on_delete=models.CASCADE)

class DatosDeContacto(models.Model):
  mail = models.EmailField(max_length=254)
  telefono = models.IntegerField()
  direccion = models.CharField(max_length=50)

class ExperienciaLaboral(models.Model):
  trabajo = models.CharField(max_length=50)
  fecha_inicio = models.DateField(auto_now=False, auto_now_add=False)
  fecha_final = models.DateField(auto_now=False, auto_now_add=False, blank=True)
  nombre_de_empresa = models.CharField(max_length=50)
  organizacion = models.CharField(max_length=50, blank=True)
  tareas_principales = models.CharField(max_length=1000)

class Educacion(models.Model):
  institucion = models.CharField(max_length=50)
  fecha_inicio = models.DateField(auto_now=False, auto_now_add=False)
  fecha_final = models.DateField(auto_now=False, auto_now_add=False, blank=True)

class Curso(models.Model):
  institucion = models.CharField(max_length=50)
  fecha_inicio = models.DateField(auto_now=False, auto_now_add=False)
  fecha_final = models.DateField(auto_now=False, auto_now_add=False, blank=True)
