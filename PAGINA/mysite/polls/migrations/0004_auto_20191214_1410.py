# Generated by Django 3.1.dev20191213103809 on 2019-12-14 17:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0003_cursos_datosdecontacto_educacion_experiencialaboral'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Cursos',
            new_name='Curso',
        ),
    ]
