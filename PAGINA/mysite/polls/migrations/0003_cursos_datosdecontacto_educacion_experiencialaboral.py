# Generated by Django 3.1.dev20191213103809 on 2019-12-14 15:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0002_auto_20191214_1058'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cursos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('institucion', models.CharField(max_length=50)),
                ('fecha_inicio', models.DateField()),
                ('fecha_final', models.DateField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='DatosDeContacto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mail', models.EmailField(max_length=254)),
                ('telefono', models.IntegerField()),
                ('direccion', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Educacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('institucion', models.CharField(max_length=50)),
                ('fecha_inicio', models.DateField()),
                ('fecha_final', models.DateField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ExperienciaLaboral',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('trabajo', models.CharField(max_length=50)),
                ('fecha_inicio', models.DateField()),
                ('fecha_final', models.DateField(blank=True)),
                ('nombre_de_empresa', models.CharField(max_length=50)),
                ('organizacion', models.CharField(blank=True, max_length=50)),
                ('tareas_principales', models.CharField(max_length=1000)),
            ],
        ),
    ]
