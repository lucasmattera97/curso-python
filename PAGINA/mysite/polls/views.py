from django.shortcuts import render
from django.http import HttpResponse
from .models import *
from django.template import loader

# Create your views here.

def index(request):
    datos_personales_ls = DatosPersonales.objects.all()[:3]

    datos_de_contacto_ls = DatosDeContacto.objects.all()[:3]

    experiencia_laboral_ls = ExperienciaLaboral.objects.all()[:3]

    educacion_ls = Educacion.objects.all()[:3]

    curso_ls = Curso.objects.all()[:3]

    hobbies_ls = HobbiesDe.objects.all()[:3]

    context = {
        'datos_personales_ls': datos_personales_ls,
        'datos_de_contacto_ls': datos_de_contacto_ls,
        'experiencia_laboral_ls': experiencia_laboral_ls,
        'educacion_ls': educacion_ls,
        'curso_ls': curso_ls,
        'hobbies_ls': hobbies_ls,
        }
    return render(request, 'polls/index.html', context)

def resumen(request):
    resumen = DatosPersonales.objects.all()
    return render(request, 'polls/vista_resumen.html', {'resumen' : resumen})

def experiencia_total(request):
    experiencia_laboral_ls = ExperienciaLaboral.objects.all()
    return render(request, 'polls/vista_xp.html', {'experiencia_laboral_ls' : experiencia_laboral_ls})

def educacion_total(request):
    educacion_ls = Educacion.objects.all()
    return render(request, 'polls/vista_edu.html', {'educacion_ls' : educacion_ls})

def cursos_total(request):
    curso_ls = Curso.objects.all()
    return render(request, 'polls/vista_cursos.html', {'curso_ls' : curso_ls})

def hobbies_total(request):
    hobbies_ls = HobbiesDe.objects.all()
    return render(request, 'polls/vista_hobbies.html', {'hobbies_ls' : hobbies_ls})
