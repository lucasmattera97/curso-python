from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('experiencia/', views.experiencia_total, name='experiencia_total'),
    path('educacion/', views.educacion_total, name='educacion_total'),
    path('cursos/', views.cursos_total, name='cursos_total'),
    path('hobbies/', views.hobbies_total, name='hobbies_total'),
    path('resumen/', views.resumen, name='resumen')

]