from django.contrib import admin
from django.contrib import admin

from .models import Hobbies, HobbiesDe, DatosPersonales, DatosDeContacto, ExperienciaLaboral,Educacion,Curso

admin.site.register(DatosPersonales)
admin.site.register(DatosDeContacto)
admin.site.register(ExperienciaLaboral)
admin.site.register(Educacion)
admin.site.register(Curso)
admin.site.register(Hobbies)
admin.site.register(HobbiesDe)
# Register your models here.
