"""
• Un desafío sólo para valientes: el juego de adivinar el nro, pero al reves. 
Es decir, que el usuario seleccione un número, y que la computadora intente adivinarlo a partir de las sugerencias del usuario. 
Sugerencia: escribir la planificación del programa primero. Eventualmente si no logran armar el programa, 
envíen al menos la planificación del mismo en pseudocódigo como hicimos la otra vez.
"""