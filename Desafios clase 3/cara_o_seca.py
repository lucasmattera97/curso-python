"""
• Escribir un programa que arroje una moneda 100 veces e indique la cantidad de veces que tocó cara y la cantidad
 de veces que tocó seca.
 """
import random 
cara = 1
seca = 2
lado = random.randint(cara, seca)
lanzamientos = 0
vecesCara = 0
vecesSeca = 0

while lanzamientos < 100:
	lanzamientos += 1
	lado = random.randint(cara, seca)
	if lado == cara:
		vecesCara += 1
	else:
		vecesSeca =+ 1

print('cara cayo', vecesCara, 'veces')
print('seca cayo', vecesSeca, 'veces')