import tkinter as tk

class Creador(tk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.grid()
        self.crear_componentes()

    def crear_componentes(self):
        self.lbl_instrucciones = tk.Label(
            self, text="Ingresar informacion para la nueva historia"
        )
        self.lbl_instrucciones.grid(
            row=0,
            column=0,
            columnspan=2,
            sticky=tk.W
        )

        self.lbl_Nombre = tk.Label(
            self, text="Nombre: "
        )
        self.lbl_Nombre.grid(
            row=1,
            column=0,
            sticky=tk.W
        )

        self.lbl_sustantivo = tk.Label(
            self, text="Sustantivo en plural: "
        )
        self.lbl_sustantivo.grid(
            row=2,
            column=0,
            sticky=tk.W
        )

        self.lbl_verbo = tk.Label(
            self, text="Verbo: "
        )
        self.lbl_verbo.grid(
            row=3,
            column=0,
            sticky=tk.W
        )

        self.lbl_adjetivo = tk.Label(
            self, text="Adjetivo(s): "
        )
        self.lbl_adjetivo.grid(
            row=4,
            column=0,
            sticky=tk.W
        )

        self.lbl_parte = tk.Label(
            self, text="Parte del cuerpo: "
        )
        self.lbl_parte.grid(
            row=5,
            column=0,
            sticky=tk.W
        )

        self.btn_generar = tk.Button(
            self,
            text="Generar la historia",
            command=self.generar
        )
        self.btn_generar.grid(
            row=6,
            column=0,
            sticky=tk.W
        )

        self.txt_nombre = tk.Entry(self)
        self.txt_nombre.grid(
            row=1,
            column=1,
            sticky=tk.W
        )

        self.txt_sustantivo = tk.Entry(self)
        self.txt_sustantivo.grid(
            row=2,
            column=1,
            sticky=tk.W
        )

        self.txt_verbo = tk.Entry(self)
        self.txt_verbo.grid(
            row=3,
            column=1,
            sticky=tk.W
        )

        self.txt_historia = tk.Text(
            self,
            width=60,  
            height=18,
            wrap=tk.WORD
        ) 
        self.txt_historia.grid(
            row=7,
            column=0,
            columnspan=5,
            sticky=tk.W
        )

        self.eligio_ansioso = tk.BooleanVar()
        tk.Checkbutton(
            self,
            text="Ansioso",
            variable=self.eligio_ansioso,
        ).grid(
            row=4, column=1, sticky=tk.W
        )

        self.eligio_alegre = tk.BooleanVar()
        tk.Checkbutton(
            self,
            text="Alegre",
            variable=self.eligio_alegre,
        ).grid(
            row=4, column=2, sticky=tk.W
        )

        self.eligio_inquieto = tk.BooleanVar()
        tk.Checkbutton(
            self,
            text="Inquieto",
            variable=self.eligio_inquieto,
        ).grid(
            row=4, column=3, sticky=tk.W
        )

        self.parte = tk.StringVar()
        self.parte.set(None)

        tk.Radiobutton(
            self,
            text="pepe",
            variable=self.parte,
            value="brazo",
        ).grid(
            row=5, column=1, sticky=tk.W
        )

        tk.Radiobutton(
            self,
            text="Pie",
            variable=self.parte,
            value="pie",
        ).grid(
            row=5, column=2, sticky=tk.W
        )

        tk.Radiobutton(
            self,
            text="Cachete",
            variable=self.parte,
            value="cachete",
        ).grid(
            row=5, column=3, sticky=tk.W
        )

    def generar(self):
        nombre = self.txt_nombre.get()
        sustantivo = self.txt_sustantivo.get()
        verbo = self.txt_verbo.get()
        adjetivos = []
        parte = self.parte.get()

        if self.eligio_alegre.get():
            adjetivos.append('alegre')

        if self.eligio_ansioso.get():
            adjetivos.append('ansioso')

        if self.eligio_inquieto.get():
            adjetivos.append('inquieto')

        mensaje = """ La famosa expedición de {} estuvo a punto de abandonar una búsqueda de casi toda la vida: la de la Ciudad Perdida de {}. """.format(nombre, sustantivo)

        mensaje += """Sin embargo un día, una turba de {} encontró a {} en persona. Si Mahoma no va a la montaña, la montaña va a Mahoma ;-) """.format(sustantivo, nombre)

        mensaje +=  """Un fuerte, """
        
        for a in adjetivos:
            mensaje += a+', '
        
        mensaje += """peculiar sentimiento abrumó a {}. """.format(nombre)

        mensaje += """Después de todo este tiempo, la búsqueda al fin terminó. """

        mensaje += """Una lágrima cayó sobre el {} de {}. """.format(parte, nombre)

        mensaje += """Luego, el grupo de {} rápidamente se comió a {}. """.format(sustantivo, nombre)

        mensaje += """¿La moraleja de la historia? Cuidado con lo que quieras {}. """.format(verbo)

        self.txt_historia.delete(0.0, tk.END)
        self.txt_historia.insert(0.0, mensaje)


# Crear ventana root
root = tk.Tk()

# Modificar la ventana
root.title("Creador de historias")
root.geometry("600x370")

# Creamos un frame en la ventana
frame = Creador(root)

# Lanzar el ciclo de eventos
root.mainloop()